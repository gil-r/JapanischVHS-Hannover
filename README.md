# Japanisch
## VHS Hannover Kurs Nr 77405M8
## Dozent: Shigesada-Kruse, Yoshimi

# Anki Setup
* [Anki Desktiop](https://apps.ankiweb.net/)
* [Anki Android](https://play.google.com/store/apps/details?id=com.ichi2.anki&gl=DE)
  * Preis: Kostenlos (Stand 05.04.2020)
* [Anki iOS](https://apps.apple.com/us/app/ankimobile-flashcards/id373493387)
  * Preis: 27,99 € (Stand 05.04.2020)
* [AnkiWeb](https://ankiweb.net/account/login) 
  * Der Account ist nötig um über mehrere Geräte zu synchronisieren bzw. die Sammlung in die Clound zu sichern.
* Plugin [CrowdAnki](https://ankiweb.net/shared/info/1788670778)
  * Mit diesem Plugin kann man ein Deck als JSON Datei exportieren, was es den Umgang in Git vereinfacht.
* [Tutorial](https://www.youtube.com/watch?v=5urUZUWoTLo&t)

# Anki Tips
* [Karten importieren](https://docs.ankiweb.net/#/importing)
  * [via Spreadsheet](https://www.reddit.com/r/Anki/comments/2wnrrw/adding_multiple_cards_to_anki_at_once_can_you_do/)

# getestete Decks
* [Genki 1 & 2, Incl Genki 1 Supplementary Vocab](https://ankiweb.net/shared/info/942922371)
* [Genki 1 & 2 Kanji](https://ankiweb.net/shared/info/1557570150) 
* [Japanese verbs for beginners with all useful forms](https://ankiweb.net/shared/info/599668177)

# interessante Decks
* [Japanese verbs ~te form](https://ankiweb.net/shared/info/1105814570)

# sonstige Ressourcen
* [Genki Website](http://genki.japantimes.co.jp/self_en)
  * Hier gibt es links zu Vokabeltests, Videos und mehr
* [Genki Übungen (mit Ton)](https://sethclydesdale.github.io/genki-study-resources/#lesson-7)
* [jpn-language](https://jpn-language.com/)
  * Hier gibt es verschiedenes Lehrmaterial
  * unter anderem Vokabeln mit [Bildern](https://jpn-language.com/category/learn-japanese/useful-vocabulary) dazu
* [Grammatik](http://www.guidetojapanese.org/learn/grammar)
* [6000 häufigste Wörter](https://docs.google.com/spreadsheets/d/13PjfuQSsbkixNGj_9U271M6eqGVVWp5ATRivKlN06S4/edit#gid=0)

## Grammatik
* [Partikel](http://www.guidetojapanese.org/particles2.html)
* [は　vs　が](https://8020japanese.com/wa-vs-ga/) 

# Tools
* [Kanji - Hiragana - Romaji Konverter](https://j-talk.com/convert)
